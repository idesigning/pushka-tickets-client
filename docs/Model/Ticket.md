# Ticket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID билета | 
**barcode** | **string** | ШК билета (QR-код) | 
**status** | [**\Pushka\Client\Model\Status**](Status.md) |  | 
**visitor** | [**\Pushka\Client\Model\Visitor**](Visitor.md) |  | 
**buyer** | [**\Pushka\Client\Model\Buyer**](Buyer.md) |  | 
**session** | [**\Pushka\Client\Model\Session**](Session.md) |  | 
**payment** | [**\Pushka\Client\Model\Payment**](Payment.md) |  | 
**visit_date** | **int** | Дата посещения (гашения) (unix timestamp) | [optional] 
**refund_date** | **int** | Дата возврата билета (unix timestamp) | [optional] 
**refund_reason** | **string** | Причина возврата | [optional] 
**refund_rrn** | **string** | RRN (Retrieval Reference Number) - уникальный идентификатор транзакции возврата | [optional] 
**refund_ticket_price** | **string** | Сумма возврата | [optional] 
**comment** | **string** | Комментарий (для билета) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

