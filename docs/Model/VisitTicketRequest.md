# VisitTicketRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visit_date** | **int** | Дата посещения (гашения) (unix timestamp) | 
**comment** | **string** | Комментарий | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

