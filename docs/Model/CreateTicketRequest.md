# CreateTicketRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**barcode** | **string** | ШК билета (QR-код) | 
**visitor** | [**\Pushka\Client\Model\Visitor**](Visitor.md) |  | 
**buyer** | [**\Pushka\Client\Model\Buyer**](Buyer.md) |  | 
**session** | [**\Pushka\Client\Model\Session**](Session.md) |  | 
**payment** | [**\Pushka\Client\Model\Payment**](Payment.md) |  | 
**comment** | **string** | Комментарий (для билета) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

