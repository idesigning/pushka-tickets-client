# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_id** | **string** | ID мероприятия в PRO.Культура | 
**organization_id** | **string** | ID организации в Про.Культура | 
**date** | **int** | Дата/Время проведения сеанса (unix timestamp) | 
**place** | **string** | Адрес/описание места проведения мероприятия | [optional] 
**params** | **string** | Зал+Сектор+Ряд+Место | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

