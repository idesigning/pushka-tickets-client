# Buyer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mobile_phone** | **string** | Мобильный телефон (10 цифр) | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

