# Pushka\Client\_Api

All URIs are relative to *https://pushka-uat.test.gosuslugi.ru/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventsEventIdTicketsBarcodeGet**](_Api.md#eventseventidticketsbarcodeget) | **GET** /events/{event_id}/tickets/{barcode} | Получение информации о сеансе по билету
[**eventsEventIdTicketsBarcodeVisitPut**](_Api.md#eventseventidticketsbarcodevisitput) | **PUT** /events/{event_id}/tickets/{barcode}/visit | Погасить билет
[**ticketsIdGet**](_Api.md#ticketsidget) | **GET** /tickets/{id} | Получение информации о билете
[**ticketsIdRefundPut**](_Api.md#ticketsidrefundput) | **PUT** /tickets/{id}/refund | Вернуть билет
[**ticketsIdVisitPut**](_Api.md#ticketsidvisitput) | **PUT** /tickets/{id}/visit | Погасить билет
[**ticketsPost**](_Api.md#ticketspost) | **POST** /tickets | Добавление билета в реестр

# **eventsEventIdTicketsBarcodeGet**
> \Pushka\Client\Model\TicketInfo eventsEventIdTicketsBarcodeGet($event_id, $barcode)

Получение информации о сеансе по билету

Получение информации о сеансе по QR и ID события

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Pushka\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Pushka\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$event_id = "event_id_example"; // string | ID мероприятия в ПРО.Культура
$barcode = "barcode_example"; // string | ШК билета

try {
    $result = $apiInstance->eventsEventIdTicketsBarcodeGet($event_id, $barcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->eventsEventIdTicketsBarcodeGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **event_id** | **string**| ID мероприятия в ПРО.Культура |
 **barcode** | **string**| ШК билета |

### Return type

[**\Pushka\Client\Model\TicketInfo**](../Model/TicketInfo.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **eventsEventIdTicketsBarcodeVisitPut**
> \Pushka\Client\Model\VisitResult eventsEventIdTicketsBarcodeVisitPut($body, $event_id, $barcode)

Погасить билет

Добавить в билет информацию о посещении

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Pushka\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Pushka\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Pushka\Client\Model\VisitTicketRequest(); // \Pushka\Client\Model\VisitTicketRequest | 
$event_id = "event_id_example"; // string | ID мероприятия в ПРО.Культура
$barcode = "barcode_example"; // string | ШК билета

try {
    $result = $apiInstance->eventsEventIdTicketsBarcodeVisitPut($body, $event_id, $barcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->eventsEventIdTicketsBarcodeVisitPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Pushka\Client\Model\VisitTicketRequest**](../Model/VisitTicketRequest.md)|  |
 **event_id** | **string**| ID мероприятия в ПРО.Культура |
 **barcode** | **string**| ШК билета |

### Return type

[**\Pushka\Client\Model\VisitResult**](../Model/VisitResult.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketsIdGet**
> \Pushka\Client\Model\Ticket ticketsIdGet($id)

Получение информации о билете

Запросить билет по ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Pushka\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Pushka\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | ID билета

try {
    $result = $apiInstance->ticketsIdGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->ticketsIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| ID билета |

### Return type

[**\Pushka\Client\Model\Ticket**](../Model/Ticket.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketsIdRefundPut**
> \Pushka\Client\Model\RefundResult ticketsIdRefundPut($body, $id)

Вернуть билет

Добавить информацию о возврате билета

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Pushka\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Pushka\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Pushka\Client\Model\RefundTicketRequest(); // \Pushka\Client\Model\RefundTicketRequest | 
$id = "id_example"; // string | ID билета

try {
    $result = $apiInstance->ticketsIdRefundPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->ticketsIdRefundPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Pushka\Client\Model\RefundTicketRequest**](../Model/RefundTicketRequest.md)|  |
 **id** | **string**| ID билета |

### Return type

[**\Pushka\Client\Model\RefundResult**](../Model/RefundResult.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketsIdVisitPut**
> \Pushka\Client\Model\VisitResult ticketsIdVisitPut($body, $id)

Погасить билет

Добавить в билет информацию о посещении

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Pushka\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Pushka\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Pushka\Client\Model\VisitTicketRequest(); // \Pushka\Client\Model\VisitTicketRequest | 
$id = "id_example"; // string | ID билета

try {
    $result = $apiInstance->ticketsIdVisitPut($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->ticketsIdVisitPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Pushka\Client\Model\VisitTicketRequest**](../Model/VisitTicketRequest.md)|  |
 **id** | **string**| ID билета |

### Return type

[**\Pushka\Client\Model\VisitResult**](../Model/VisitResult.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticketsPost**
> \Pushka\Client\Model\Ticket ticketsPost($body)

Добавление билета в реестр

Добавить в реестр информацию о билете, купленном по Пушкинской карте

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Pushka\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Pushka\Client\Api\_Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Pushka\Client\Model\CreateTicketRequest(); // \Pushka\Client\Model\CreateTicketRequest | 

try {
    $result = $apiInstance->ticketsPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling _Api->ticketsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Pushka\Client\Model\CreateTicketRequest**](../Model/CreateTicketRequest.md)|  |

### Return type

[**\Pushka\Client\Model\Ticket**](../Model/Ticket.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

