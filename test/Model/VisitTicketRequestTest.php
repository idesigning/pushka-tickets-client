<?php
/**
 * VisitTicketRequestTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Pushka\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Работа с билетами
 *
 * API для передачи информации в реестр сведений о билетах
 *
 * OpenAPI spec version: 0.1.5
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.29
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Pushka\Client;

/**
 * VisitTicketRequestTest Class Doc Comment
 *
 * @category    Class
 * @description VisitTicketRequest
 * @package     Pushka\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class VisitTicketRequestTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "VisitTicketRequest"
     */
    public function testVisitTicketRequest()
    {
    }

    /**
     * Test attribute "visit_date"
     */
    public function testPropertyVisitDate()
    {
    }

    /**
     * Test attribute "comment"
     */
    public function testPropertyComment()
    {
    }
}
