# VisitResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visit_date** | **int** | Дата погашения билета (unix timestamp) | 
**status** | [**\Pushka\Client\Model\Status**](Status.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

