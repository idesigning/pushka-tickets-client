# RefundResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refund_date** | **int** | Дата возврата билета (unix timestamp) | 
**refund_reason** | **string** | Причина возврата | 
**refund_rrn** | **string** | RRN (Retrieval Reference Number) - уникальный идентификатор транзакции возврата | [optional] 
**refund_ticket_price** | **string** | Сумма возврата | [optional] 
**status** | [**\Pushka\Client\Model\Status**](Status.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

