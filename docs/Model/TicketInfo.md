# TicketInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**\Pushka\Client\Model\Status**](Status.md) |  | [optional] 
**session** | [**\Pushka\Client\Model\Session**](Session.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

