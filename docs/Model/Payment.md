# Payment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | ID платежа у Билетного оператора | [optional] 
**rrn** | **string** | RRN (Retrieval Reference Number) - уникальный идентификатор транзакции | [optional] 
**date** | **int** | Дата/время совершения платежа (unix timestamp) | 
**ticket_price** | **string** | Цена билета (номинал) | [optional] 
**amount** | **string** | Сумма платежа по Пушкинской карте | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

