# Visitor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**full_name** | **string** | ФИО (целиком) | 
**first_name** | **string** | Имя | [optional] 
**middle_name** | **string** | Отчество | [optional] 
**last_name** | **string** | Фамилия | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

